class AddIconToDocument < ActiveRecord::Migration[5.0]
  def change
    add_column :documents, :icon, :binary
  end
end
