class ImageProcessingWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable
  include DocumentsHelper

  recurrence { hourly }

  def perform
    import_images_and_ocr
  end
end