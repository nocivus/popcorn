require "base64"
require 'net/http'
require 'json'

module ApplicationHelper
  def show_flash(kind)
    data = ""
    if flash[kind]
      data << "<div id=\"flash-#{kind}\">"
      if flash[kind].is_a? Array
        data << "<ul>"
        flash[kind].each do |f|
          data << "<li>#{f}</li>"
        end
        data << "</ul>"
      else
        data << flash[kind]
      end
      data << "</div>"
    end
    data.html_safe
  end
end
