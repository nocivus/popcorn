json.array!(@documents) do |document|
  json.extract! document, :id, :created_at
  json.url document_url(document, format: :json)
end
