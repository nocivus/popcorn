require "base64"
require 'rmagick'
include Magick

class Document < ApplicationRecord

  validates :image, presence: true
  validates :filename, presence: true
  validates :content_type, presence: true

  default_scope { order('created_at desc') }
  scope :unprocessed, -> { where(ocr_text: nil) }

  def initialize(params = {})
    puts "Instantiating from the web: #{params[:file]}"
    file = params.delete(:file)
    super
    if file
      Document.generate_doc(self, file.original_filename, file.tempfile)
    end
  end

  def self.from_file(filename)
    puts "Instantiating from file: #{filename}"
    doc = Document.new
    file = File.open(filename, 'r')
    Document.generate_doc(doc, filename, file)
  end

  def self.generate_doc(doc, filename, file)
    if file
      doc.filename = doc.sanitize_filename(filename)
      puts "Encoding base64 data"
      doc.content_type, doc.image = Document.generate_base_64(file.path)
    end    
    doc
  end

  def miniature
    unless self[:miniature]
      mini = Image.from_blob(Base64.decode64(self.image)).first
      mini = mini.resize_to_fit(100, 100)
      mini.format = "PNG"
      mini_data = mini.to_blob
      self.update_attributes!(miniature: Base64.encode64(mini_data))
    end
    self[:miniature]
  end

  def icon
    unless self[:icon]
      mini = Image.from_blob(Base64.decode64(self.image)).first
      mini = mini.resize_to_fit(16, 16)
      mini.format = "PNG"
      mini_data = mini.to_blob
      self.update_attributes!(icon: Base64.encode64(mini_data))
    end
    self[:icon]
  end

  def self.generate_base_64(filename)
    img = Image.read(filename) do
      self.quality = 100
      self.density = '300'
    end.first
    img.format = "PNG"
    img = img.resize_to_fit(2000, 2000)
    img_data = img.to_blob
    return "image/png", Base64.encode64(img_data)
  end

  def text_as_html
    return '' unless ocr_text?
    ocr_text.gsub("\n", '<br/>')
            .gsub("\r", '<br/>')
            .html_safe
  end

  def sanitize_filename(filename)
    # Get only the filename, not the whole path (for IE)
    # Thanks to this article I just found for the tip: http://mattberther.com/2007/10/19/uploading-files-to-a-database-using-rails
    return File.basename(filename)
  end
end
