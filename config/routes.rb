Rails.application.routes.draw do

  devise_for :users
  require 'sidekiq/web' 
  mount Sidekiq::Web => '/jobs'
  
  get "/documents/:id/image" => "documents#show_image", as: :image
  get "/documents/import" => "documents#import_now", as: :import_now

  resources :tasks, :documents

  root 'pages#main'
end
