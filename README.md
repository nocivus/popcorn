## README

This is a project that finds documents in a specified folder (configured in config/environments/development.rb and config/environments/production.rb), converts them to png (using imagemagick), stores them in the database, and finally asks Google Cloud Vision API to perform OCR on them.

The documents can then be search by OCR text in the web interface.

How to get this up and running:

Requirements:

* Ruby 2.3.0+
* rbenv to manage the ruby versions
* rbenv-vars plugin to manage environment variables
* Redis Server (for the background image processing and OCR job)
* ImageMagick (and probably a few other dependencies, like graphviz, libsvg, libcairo, etc.)
* A Google Cloud Vision API key

Setup:

* Change the file config/environment_variables.rb to change the production document import folder location (and development, if you want)
* Create an .rbenv-vars file with your Google Vision API key, like so:
```
#!ruby
GOOGLE_VISION_API_KEY=your_key
```
* In addition, in production, you will want your db credentials in this file as well
```
#!ruby
POPCORN_DATABASE_USER=your_db_username
POPCORN_DATABASE_PASSWORD=your_db_password
```
* bundle install
* rails db:create
* rails db:migrate
* rails console (and create a user with User.create!(username: your_username, password: your_password))
* rails s (for development)

Deployment:

* Edit the config/deploy.rb file for your own Capistrano options for deployment.
* run cap production deploy once you have a place to put it and deploy options are correct

Notes:

* It uses sidekiq to manage the background job, along with redis
* Currently setup to use postgresql, but can be hacked to use whatever dbms you want
* Currently stores images in the database, but this can also be changed in the future.

How i use it:

* I have a folder in my Dropbox account to which i upload the documents i want scanned and OCR'ed
* The server reads this folder from times to times, imports the document, uses Google to OCR it and deletes the file from the dropbox folder.

Feel free to change it and if you make some nice stuff please let me know :)