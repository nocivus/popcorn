class CreateStructureForDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :documents do |t|

      t.string :filename
      t.string :content_type
      
      t.binary :image
      t.binary :miniature

      t.text :ocr_text

      t.timestamps
    end
  end
end
