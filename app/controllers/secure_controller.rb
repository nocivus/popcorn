include ActionView::Helpers::UrlHelper

class SecureController < ApplicationController
  before_action :authenticate_user!
  before_action :set_link_to

  def set_link_to
    title = controller_name.classify.downcase.pluralize
    @current_link = title ? link_to(title, title.to_sym) : nil
  end
end
