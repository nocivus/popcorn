module DocumentsHelper

  def miniature_for(doc)
    "<img src=\"data:image/png;base64,#{doc.miniature}\" />".html_safe
  end

  def icon_for(doc)
    "<img src=\"data:image/png;base64,#{doc.icon}\" />".html_safe
  end

  def import_images_and_ocr
    puts "Processing images from : #{ENV['IMAGES_IMPORT_FOLDER']}"

    # Read images from import folder
    Dir["#{ENV['IMAGES_IMPORT_FOLDER']}/*"].each do |f|
      if Document.from_file(f).save
        File.delete(f)
      end
    end

    # OCR those that need to be ocr'ed
    Document.unprocessed.each do |d|
      d.update_attributes!(ocr_text: ocr_from_google(d.image))
    end
  end

  def ocr_from_google(base64_data)
    # Stuff we need
    api_key = ENV['GOOGLE_VISION_API_KEY']
    content_type = "Content-Type: application/json"
    url = "https://vision.googleapis.com/v1/images:annotate?key=#{api_key}"
    data = {
      "requests": [
        {
          "image": {
            "content": base64_data
          },
          "features": [
            {
              "type": "TEXT_DETECTION",
              "maxResults": 1
            }
          ]
        }
      ]
    }.to_json

    # Make the request
    url = URI(url)
    req = Net::HTTP::Post.new(url, initheader = {'Content-Type' =>'application/json'})
    req.body = data
    res = Net::HTTP.new(url.host, url.port)
    res.use_ssl = true

    # res.set_debug_output $stderr

    detected_text = ""
    res.start do |http| 
      puts "Querying Google for image: #{ARGV[0]}"
      resp = http.request(req)
      json = JSON.parse(resp.body)
      if json && json["responses"] && json["responses"][0]["textAnnotations"] && json["responses"][0]["textAnnotations"][0]["description"]
        detected_text = json["responses"][0]["textAnnotations"][0]["description"]
      else
        puts "Ooopsie from Google:"
        pp json
      end
    end

    puts "Google says the image is: #{detected_text}"
    detected_text
  end
end
